var express = require("express");
var request = require("request");
var cheerio = require("cheerio");
var cors = require("cors");
var fs = require("fs");

var Server = express();
const PORT = process.env.SERVER_PORT;

Server.use(cors());
Server.use(express.json());

Server.get("/ebay-page/:index", function (req, res) {
  var index = req.params.index;
  var URL_LIST =
    "https://www.ebay.com/b/E-Mail-Computer-Software/18793/bn_871154?LH_FS=1&LH_PrefLoc=3&rt=nc&_pgn=" +
    index;
  request(URL_LIST, function (error, response, html) {
    if (!error) {
      var $ = cheerio.load(html);
      var result = [];
      $("#mainContent .b-list__items_nofooter .s-item__wrapper").each(
        (i, item) => {
          var href = $(item)
            .find("a[data-track]", 0)
            .attr("href")
            .split("?hash=", 1)[0];
          const isItem = href.indexOf("https://www.ebay.com/itm/") > -1;
          if (isItem) {
            var id = href.split("/").filter(Boolean).pop();
            var src = $(item).find(".s-item__image-img").attr("src");
            var title = $(item).find(".s-item__title").text().trim();
            var hotness = $(item)
              .find(".s-item__hotness .NEGATIVE")
              .text()
              .trim();
            result.push({ id, href, title, src, hotness });
          }
        }
      );
    }
    res.status(202).json({ result });
  });
});

Server.post("/ebay-item", function (req, res) {
  var url;
  if (req.body) url = req.body.url;
  if (url) {
    request(url, function (error, response, html) {
      if (!error) {
        var $ = cheerio.load(html);
        var sold = $("#why2buy > div > div:nth-child(2) > span.w2b-head")
          .text()
          .trim()
          .replace(",", "");
        var qtyHot = $("#mainContent .qtyCntVal .vi-qtyS-hot-red")
          .text()
          .trim();
        var qtyTxt = $("#mainContent .qtyCntVal .qtyTxt").text().trim();
        res.status(202).json({ sold, qtyHot, qtyTxt });
      } else res.status(500).json({ result: "hello item error" });
    });
  } else res.status(500).json({ result: "hello empty url!" });
});

Server.get("/ebay-data", function (req, res) {
  fs.writeFile("output.json", JSON.stringify({ result }, null, 4), function (
    err
  ) {
    console.log(
      "File successfully written! - Check your project directory for the output.json file"
    );
  });
});

Server.listen(PORT);
console.log("Listen on port", PORT);
exports = module.exports = Server;
