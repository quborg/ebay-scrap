import React, { useState, useEffect } from "react";
import cheerio from "cheerio";

import "./App.css";

function App() {
  const [result, setResult] = useState([]);

  useEffect(() => {
    async function fetchEbayPages() {
      const URL_LIST = "http://localhost:3001/ebay-page/";
      const URL_ITEM = "http://localhost:3001/ebay-item/";
      let _result = [];
      for (let i = 1; i < 25; i++) {
        const response = await fetch(URL_LIST + i);
        const json = await response.json();
        json.result.forEach((item, j) => {
          async function fetchItem() {
            const data = { url: item.href };
            const _response = await fetch(URL_ITEM, {
              method: "POST",
              headers: { "Content-Type": "application/json" },
              body: JSON.stringify(data),
            });
            const _json = await _response.json();
            _result = [..._result, { ...item, ..._json }].sort(function (a, b) {
              let flag = 0;
              const aSold = Number(a.sold);
              const bSold = Number(b.sold);
              if (isNaN(aSold) || isNaN(bSold)) {
                if (!isNaN(aSold) && isNaN(bSold)) flag = -1;
                if (isNaN(aSold) && !isNaN(bSold)) flag = 1;
              }
              if (!isNaN(aSold) && !isNaN(bSold)) {
                if (aSold > bSold) flag = -1;
                if (aSold < bSold) flag = 1;
              }
              return flag;
            });
            setResult(_result.slice(0, 49));
          }
          fetchItem();
        });
      }
    }
    fetchEbayPages();
  }, []);

  return (
    <section>
      <div className="count">
        <span>{result.length}</span>
      </div>
      <div className="content">
        {result.map((item) => (
          <div
            key={`${item.id}-${item.title}`}
            className="item"
            onClick={() => window.open(item.href)}
          >
            <img src={item.src} alt={item.title} />
            <div className="details">
              <div className="hot">
                <span>{item.sold}</span>
                <span>{item.qtyHot}</span>
                <span>{item.qtyTxt}</span>
                <span>{item.hotness}</span>
              </div>
              <div className="title">
                <span>{item.title}</span>
              </div>
            </div>
          </div>
        ))}
      </div>
    </section>
  );
}

export default App;
